use master;
GO

if exists (select name from sys.databases where name = N'WebShop') drop database WebShop;

create database WebShop
go

use WebShop;
go

create table Customer(
	customerId int not null primary key IDENTITY(1,1),
	firstName nvarchar(50) not null,
	lastName nvarchar(50) not null,
	eMail nvarchar(50) not null,
	address nvarchar(500) not null,
	phone int
)

create table [Order](
	orderId int not null primary key IDENTITY(1,1),
	customerId int not null,
)

create table OrderLines(
	orderLineId int not null primary key IDENTITY(1,1),
	orderId int not null,
	productId int not null,
	weight float not null
)

create table Product(
	productId int not null primary key IDENTITY(1,1),
	title nvarchar(100) not null,
	description ntext,
	pictures nvarchar(500),
	variant nvarchar(100),
	type nvarchar(100),
	supplier nvarchar(100) not null,
	brand nvarchar(100),
	country nvarchar(100),
	fatProcentage float,
	aging int,
	moldy bit,
	color nvarchar(100),
	firmness nvarchar(100),
	kgPrice float,
)
go

alter table [Order]
add foreign key (customerId) references Customer(customerId);
go

alter table OrderLines
add foreign key (orderId) references [Order](orderId);
alter table OrderLines
add foreign key (productId) references Product(productId);
go







