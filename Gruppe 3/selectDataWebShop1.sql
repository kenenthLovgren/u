use WebShop
go


select [Order].orderId,[Order].customerId,[OrderLines].productId,[OrderLines].weight,Customer.firstName,Customer.lastName,Customer.eMail,[OrderLines].weight*[Product].kgPrice as "Price",Product.type
from [Order]
left outer join OrderLines on OrderLines.orderId = [Order].orderId
left outer join Customer on Customer.customerId = [Order].customerId
left outer join Product on Product.productId = [OrderLines].productId
order by Product.type
go

--select [Order].orderId,[Order].customerId,[OrderLines].productId,[OrderLines].weight,Customer.firstName,Customer.lastName,Customer.eMail,[OrderLines].weight*[Product].kgPrice as "Price",Product.title,Product.description
--from [Order]
--left outer join OrderLines on OrderLines.orderId = [Order].orderId
--left outer join Customer on Customer.customerId = [Order].customerId
--left outer join Product on Product.productId = [OrderLines].productId
--where [Order].customerId=1
--go

create view OrderView as 
select [Order].customerId,[Order].orderId,[Customer].eMail,isnull(sum([OrderLines].weight*[Product].kgPrice),0) as "Samlet pris"
from [Order]
left outer join OrderLines on OrderLines.orderId = [Order].orderId
left outer join Customer on Customer.customerId = [Order].customerId
left outer join Product on Product.productId = [OrderLines].productId
group by [Order].customerId,[Order].orderId,[Customer].eMail
go

--drop procedure getByType

create procedure getByType @tpe nvarchar(100) as
select [Order].customerId,[Order].orderId,[Customer].eMail,isnull(sum([OrderLines].weight*[Product].kgPrice),0) as "Samlet pris",Product.type
from [Order]
left outer join OrderLines on OrderLines.orderId = [Order].orderId
left outer join Customer on Customer.customerId = [Order].customerId
left outer join Product on Product.productId = [OrderLines].productId
where Product.type=@tpe
group by [Order].customerId,[Order].orderId,[Customer].eMail,Product.type;


exec getByType "Brie";
go
