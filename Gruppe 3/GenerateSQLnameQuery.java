import java.io.*;
import java.util.*;

class GenerateSQLnameQuery{
	public static void main(String[] args) throws Exception{
		int n = 500;
		Random rnd = new Random();
		String outFile = "generateNames.sql";
		ArrayList<String> firstNames = loadStringsFromFile("firstNameList.txt");
		ArrayList<String> lastNames = loadStringsFromFile("lastNameList.txt");
		PrintWriter out = new PrintWriter(outFile);
		out.println("INSERT INTO Customer");
		out.println("(firstName, lastName, eMail,address,phone)");
		out.println("VALUES");
		for(int i = 0; i< n; i++){
			String fnam = firstNames.get(rnd.nextInt(firstNames.size()));
			String lnam = lastNames.get(rnd.nextInt(lastNames.size()));
			String emailPart1 = fnam;
			switch(rnd.nextInt(3)){
				case 0:
					emailPart1 = fnam;
					break;
				case 1:
					emailPart1 = ""+fnam.charAt(0);
					break;
				case 2:
					emailPart1 = fnam.substring(0,2);
					break;
			}
			String emailPart2 = "";
			switch(rnd.nextInt(3)){
				case 0:
					emailPart2 = "";
					break;
				case 1:
					emailPart2 = "_";
					break;
				case 2:
					emailPart2 = ".";
					break;
			}
			String emailPart3 = lnam;
			switch(rnd.nextInt(3)){
				case 0:
					emailPart3 = lnam;
					break;
				case 1:
					emailPart3 = ""+lnam.charAt(0);
					break;
				case 2:
					emailPart3 = ""+lnam.substring(0,2);
					break;	
			}
			
			String emailPart4 = "@home.com";
			switch(rnd.nextInt(3)){
				case 0:
					emailPart4 = "@home.com";
					break;
				case 1:
					emailPart4 = "@gmail.com";
					break;
				case 2:
					emailPart4 = "@hotmail.com";
					break;
			}
			
			out.println("('"+fnam+"', '"+lnam+"', '"+emailPart1+emailPart2+emailPart3+emailPart4+"', 'Home','"+(rnd.nextInt(90000000)+10000000)+"'),");
		}
		out.println("GO");
		out.close();
	}
	
	
	public static ArrayList<String> loadStringsFromFile(String fileName) throws Exception{
		BufferedReader in = new BufferedReader(new FileReader(fileName));
		ArrayList<String> svar = new ArrayList<String>();
		while(in.ready()){
			svar.add(in.readLine());
		}
		in.close();
		return svar;
	}
	
}