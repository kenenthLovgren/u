import java.io.*;
import java.util.*;

class GenerateSQLorderlinesQuery{
	public static void main(String[] args) throws Exception{
		int n = 150;
		Random rnd = new Random();
		String outFile = "generateOrderLines.sql";
		PrintWriter out = new PrintWriter(outFile);
		out.println("INSERT INTO OrderLines");
		out.println("(orderId,productId,weight)");
		out.println("VALUES");
		for(int i = 0; i< n; i++){
			out.println("("+(rnd.nextInt(100)+1)+","+(rnd.nextInt(10)+1)+","+(((float)(rnd.nextInt(10000)+1))/1000.0)+"),");
		}
		out.println("GO");
		out.close();
	}
	
	
	public static ArrayList<String> loadStringsFromFile(String fileName) throws Exception{
		BufferedReader in = new BufferedReader(new FileReader(fileName));
		ArrayList<String> svar = new ArrayList<String>();
		while(in.ready()){
			svar.add(in.readLine());
		}
		in.close();
		return svar;
	}
	
}