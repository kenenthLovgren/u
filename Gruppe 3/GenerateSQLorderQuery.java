import java.io.*;
import java.util.*;

class GenerateSQLorderQuery{
	public static void main(String[] args) throws Exception{
		int n = 100;
		Random rnd = new Random();
		String outFile = "generateOrders.sql";
		PrintWriter out = new PrintWriter(outFile);
		out.println("INSERT INTO Customer");
		out.println("(customerId)");
		out.println("VALUES");
		int nKunder = 50;
		int[] kunder = new int[nKunder];
		for(int i = 0; i< nKunder; i++){
			kunder[i] = rnd.nextInt(500)+1;
		}
		for(int i = 0; i< n; i++){
			out.println("("+kunder[rnd.nextInt(nKunder)]+"),");
		}
		out.println("GO");
		out.close();
	}
	
	
	public static ArrayList<String> loadStringsFromFile(String fileName) throws Exception{
		BufferedReader in = new BufferedReader(new FileReader(fileName));
		ArrayList<String> svar = new ArrayList<String>();
		while(in.ready()){
			svar.add(in.readLine());
		}
		in.close();
		return svar;
	}
	
}