﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheeseShop
{
    public partial class Customer
    {
        public override string ToString()
        {
            return String.Format("{0} {1}", firstName, lastName.ToLower());
        }
    }
}
