﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CheeseShop
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        CheeseShopEntities cheeseShop = new CheeseShopEntities();

        public MainWindow()
        {
            InitializeComponent();


            List<Customer> customers = (from c in cheeseShop.Customers
                where c.firstName != null
                      && c.Orders.Count > 0
                select c).Take(10).ToList();

            if (customers != null)
            {
                foreach (var customer in customers)
                {
                    allCustomers.Items.Add(customer);
                }
            }
        }

        private void AllCustomers_SelectionChanged(object sender, EventArgs e)
        {
            Customer customer = allCustomers.SelectedItem as Customer;
            if (customer != null)
            {

                valFirstName.Content= customer.firstName ?? ""; 
                valLastName.Content= customer.lastName;
                valEmail.Content = customer.eMail;

                var orders = (from o in cheeseShop.Orders
                    where o.customerId == customer.customerId
                    select o).ToList();

                orderIdListView.Items.Clear();
                if (orders.Count > 0)
                {
                    foreach (Order order in orders)
                    {
                        orderIdListView.Items.Add(order.orderId);
                    }
                }

            }
        }

        private void orderIdListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //TODO: Show information about products in the order
            var item = orderIdListView.SelectedItem;
            if (item == null)
            {
                return;
            }


            int orderId = int.Parse(item.ToString());

            var products = (from p in cheeseShop.Products
                from line in cheeseShop.OrderLines
                where line.orderId == orderId && line.productId == p.productId
                select p).ToList();

        

            cheeseList.DataContext = products;

            var brandList = (from p in cheeseShop.Products
                where p.kgPrice < 160
                group p by p.brand
                into brandGroup
                select new {brands = brandGroup.Key, products = brandGroup});

            var cheeseListJoin = (from p in cheeseShop.Products
                where p.kgPrice > 160
                join line in cheeseShop.OrderLines on p.productId equals line.productId
                join order in cheeseShop.Orders on line.orderId equals order.orderId
                join customer in cheeseShop.Customers on order.customerId equals customer.customerId
                group new {line,customer } by new {line.Product,customer } into cheeseGroup 
                select new  {cheeseId = cheeseGroup.Key.Product.productId,
                cheeseName = cheeseGroup.Key.Product.title,
                totalWeight = cheeseGroup.Sum(x => x.line.weight),
                cheeseGroup.Key.customer.firstName });


            foreach (var cheese in cheeseListJoin)
            {
                Trace.WriteLine(" ======= " + cheese.cheeseName + " " + cheese.totalWeight + " " + cheese.firstName +  " ======= ");
            }
        }

        //TODO: 

        //TODO: GO HOME AND HAVE A NICE WEEKEND
    }
}
