﻿using System;
using System.Windows;
using School.Data;

namespace School
{
    /// <summary>
    /// Interaction logic for StudentForm.xaml
    /// </summary>
    public partial class StudentForm : Window
    {
        #region Predefined code

        public StudentForm()
        {
            InitializeComponent();
        }

        #endregion

        // If the user clicks OK to save the Student details, validate the information that the user has provided
        private void ok_Click(object sender, RoutedEventArgs e)
        {
            if (firstName.Text == "")
            {
                MessageBox.Show("The student must have a first name,");
                return;
            }
            // TODO: Exercise 2: Task 2a: Check that the user has provided a first name
            if (lastName.Text == "")
            {
                MessageBox.Show("The student must have a Last name,");
                return;
            }
            // TODO: Exercise 2: Task 2b: Check that the user has provided a last name
            // TODO: Exercise 2: Task 3a: Check that the user has entered a valid date for the date of birth
            var dateValid = DateTime.TryParse(dateOfBirth.Text, out DateTime result);
            if (!dateValid)
            {
                MessageBox.Show("The student must have a date of birth,");
                return;
            }

            TimeSpan difference = DateTime.Now.Subtract(DateTime.Parse(dateOfBirth.Text));
            int ageInYears = (int)(difference.Days / 365.25);

            if (ageInYears < 5)
            {
                MessageBox.Show("The student must be 5 years or older,", "To YOUNG, YOU ARE INSANE TO PUT AT SCHOOL AT THIS AGE", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            // TODO: Exercise 2: Task 3b: Verify that the student is at least 5 years old

            // Indicate that the data is valid
            this.DialogResult = true;
        }
    }
}
