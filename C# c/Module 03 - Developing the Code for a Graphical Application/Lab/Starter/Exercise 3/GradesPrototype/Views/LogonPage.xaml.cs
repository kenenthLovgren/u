﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using GradesPrototype.Data;
using GradesPrototype.Services;

namespace GradesPrototype.Views
{
    /// <summary>
    /// Interaction logic for LogonPage.xaml
    /// </summary>
    public partial class LogonPage : UserControl
    {
        public LogonPage()
        {
            InitializeComponent();
        }

        #region Event Members
        public event EventHandler LogonSuccess;
        public event EventHandler LogonFailed;

        // TODO: Exercise 3: Task 1a: Define LogonFailed event

        #endregion

        #region Logon Validation

        // TODO: Exercise 3: Task 1b: Validate the username and password against the Users collection in the MainWindow window
        private void Logon_Click(object sender, RoutedEventArgs e)
        {
            var teacher = (from Teacher t in DataSource.Teachers
                           where t.UserName == username.Text &&
                                 t.Password == password.Password
                           select t).FirstOrDefault();

            var student = (from Student s in DataSource.Students
                           where s.UserName == username.Text &&
                                 s.Password == password.Password
                           select s).FirstOrDefault();

            if (!String.IsNullOrEmpty(teacher.UserName))
            {
                SessionContext.UserID = teacher.TeacherID;
                SessionContext.UserRole = Role.Teacher;
                SessionContext.UserName = teacher.UserName;
                SessionContext.CurrentTeacher = teacher;
                LogonSuccess(this, null);
            }else if(!String.IsNullOrEmpty(student.UserName))
            {
                SessionContext.UserID = student.StudentID;
                SessionContext.UserRole = Role.Student;
                SessionContext.UserName = student.UserName;
                SessionContext.CurrentStudent = student;
                LogonSuccess(this, null);

            }
            else
            {
                LogonFailed(this, null);
            }
        }
        #endregion
    }
}
