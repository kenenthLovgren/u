﻿using SticksGame.players;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SticksGame.game
{
    class Game : IGame
    {
        private int _sticksRemainig;
        private IPlayer[] _players;
        private int turnIndex;
        private int NoOfGames;

        public Game( IPlayer player1, IPlayer player2 )
        {
            this._players = new IPlayer[2];
            this._players[0] = player1;
            this._players[1] = player2;
        }

        public void Init()
        {
            NoOfGames = 0;
            turnIndex = 0;
            foreach (IPlayer p in _players)
            {
                p.Init();
            }
        }

        public void Play()
        {
            _sticksRemainig = 15;
            foreach (IPlayer p in _players)
            {
                p.StartGame();
            }

            do
            {
                int move = _players[turnIndex].GetMove(_sticksRemainig);
                Console.WriteLine("Stick remaining " + _sticksRemainig + ", Player " + _players[turnIndex].Name + " takes " + move + " sticks");
                turnIndex = 1 - turnIndex;
                _sticksRemainig -= move;
            } while (_sticksRemainig > 0);
            _players[turnIndex].EndGame(true);
            _players[1 - turnIndex].EndGame(false);
            NoOfGames++;
        }

        public void Play(int NoOfGames)
        {
            for (int i = 0; i < NoOfGames; i++)
            {
                Play();
            }
        }

        public string getStats()
        {
            return NoOfGames + " Games played:\n" + _players[0].Name + " won " + _players[0].Wins + " Games\n" + _players[1].Name + " won " + _players[1].Wins + " Games\n";
        }
    }
}
