﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SticksGame.game
{
    interface IGame
    {

        void Init();

        void Play();

        void Play(int NoOfGames);

        string getStats();
    }
}
