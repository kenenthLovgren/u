﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SticksGame.players
{
    interface IPlayer
    {

        string Name { get; }
        int Wins { get; }


        void StartGame();

        int GetMove(int RemainingSticks);

        void EndGame(bool YouWin);

        void Init();
    }
}
