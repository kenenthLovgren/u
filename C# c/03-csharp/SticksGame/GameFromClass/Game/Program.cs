﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    class Program
    {
        public int GameCount { get; private set; }
        private IPlayer[] players = new IPlayer[2];
        private int turn;
        private int sticks;

        public Program()
        {
            GameCount = 0;
        }

        void StartGame()
        {
            ++GameCount;
            turn = 0;
            sticks = 15;
            Console.WriteLine("New Game!");
        }

        void Run()
        {
            // 1.1 Údvølg og initialiser spillere
            players[0] = new StupidPlayer(0);
            players[1] = new RandomPlayer(1);

            bool finished = false;
            do
            {
                // 2. Starte det enkelte spil
                // 2.1 Initialisere

                StartGame();
                // 2.2 Spillere Skifte til at tage pinde
                do
                {
                    Console.WriteLine(players[turn].Name + " in turn, " + sticks + " sticks remaining");
                    int move = players[turn].GetMove(sticks);
                    Console.WriteLine(players[turn].Name + " took " + move + " sticks");
                    sticks -= move;
                    turn = 1 - turn;
                    // 2.3 Slut spil nå der ikke er flere pinde
                } while (sticks > 0);
                Console.WriteLine("Player " + players[turn].Name + " won!");
                // 3. Vølg om program sluttes, ellers start igen fra 2.
                Console.WriteLine("New game (y/n) ?");
                finished = Console.ReadLine() == "n";
            } while (!finished);
        }

        static void Main(string[] args)
        {
            // 1. Initialisre program
            Program program = new Program();
            program.Run();
        }
    }
}
