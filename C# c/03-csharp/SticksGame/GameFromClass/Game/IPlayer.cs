﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Game
{
    public interface IPlayer
    {
        string Name { get; }
        int GetMove(int sticks);
    }
}
