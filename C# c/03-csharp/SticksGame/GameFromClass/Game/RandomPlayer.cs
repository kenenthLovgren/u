﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    public class RandomPlayer : StupidPlayer, IPlayer
    {
        Random rand;
        public RandomPlayer(int id) : base(id)
        {
            Name = "Random"+Name;
            rand = new Random();
        }
        public override int GetMove(int sticks) {
            return rand.Next(sticks > 3 ? 3 : sticks)+1;
        }
    }
}
