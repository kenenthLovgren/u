﻿using System;

namespace SticksGame
{
    class Program
    {
        static void Main(string[] args)
        {
            Game game = new Game();

            game.Init();

            int gameNo = 0;
            do {
                Console.WriteLine("Starting game number {0}", ++gameNo);
                game.Play();
                Console.WriteLine();
                if (gameNo % 10 == 0)
                {
                    Console.WriteLine(game.GetStat());
                    Console.Write("New game (y/n) ?");
                }
            } while(Console.ReadLine().ToLower() != "n");
            
            Console.ReadLine();
        }

        public static Player CreatePlayer(int playerId)
        {
            Player thePlayer = null;
            do
            {
                Console.WriteLine("Creating player #" + playerId);
                Console.WriteLine();
                Console.WriteLine("Specify player type:");
                Console.WriteLine(" 1-Human player");
                Console.WriteLine(" 2-Silly computer");
                Console.WriteLine(" 3-Random computer");
                Console.WriteLine(" 4-Smart computer");
                Console.WriteLine(" 5-AI computer");
                Console.WriteLine();
                Console.Write("Enter playertype (1-5): ");
                string input = Console.ReadLine();
                switch (input)
                {
                    case "1": thePlayer = new HumanPlayer(); break;
                    case "2": thePlayer = new Player(); break;
                    case "3": thePlayer = new RandomPlayer(); break;
                    case "4": thePlayer = new SmartPlayer(); break;
                    case "5": thePlayer = new AIPlayer(); break;
                }
            } while (thePlayer == null);
            return thePlayer;
        }
    }
}
