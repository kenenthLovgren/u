USE MASTER
IF NOT EXISTS(
	SELECT name
	from sys.databases
	where name = 'EDMExercise')
CREATE DATABASE EDMExercise
go

USE EDMExercise
GO

IF OBJECT_ID( 'dbo.StudentClass', 'U') IS NOT NULL
DROP TABLE dbo.StudentClass
GO

CREATE TABLE StudentClass
(
	ClassId int NOT NULL PRIMARY KEY,
	ClassValue VARCHAR(50),
	Teacher VARCHAR(50)
	)
GO

INSERT INTO StudentClass (ClassId, ClassValue)
VALUES
	(4, '4th grade'),
	(5, '5th grade')

IF OBJECT_ID( 'dbo.Students', 'U') IS NOT NULL
DROP TABLE dbo.Students
GO

CREATE TABLE Students(
	StudentID INT NOT NULL IDENTITY PRIMARY KEY,
	Name VARCHAR(50),
	Class INT FOREIGN KEY REFERENCES StudentClass
	)
GO

INSERT INTO Students (Name, Class)
VALUES
	('Kim', 4),
	('Laura', 4),
	('Keith',4),
	('Agetha', 5),
	('Steve', 5)

GO