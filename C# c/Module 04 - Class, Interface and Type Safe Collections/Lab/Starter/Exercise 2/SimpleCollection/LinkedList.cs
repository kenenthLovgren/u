﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleCollection
{
    public class LinkedList
    {
        private class ListNode
        {
            /// <summary>
            /// ref to next node in list
            /// </summary>


            private Object _item;
            public Object Item
            {
                get => _item;
                set => _item = value;
            }

            private ListNode _nextNode;
            public ListNode NextNode { get => _nextNode; set => _nextNode = value; }

            
        }

        /// <summary>
        /// First node in the list
        /// </summary>
        private ListNode _firstNode;


        /// <summary>
        /// Add A item with the 
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public object Add(Object item)
        {
            var node = new ListNode();
            node.Item = item;

            if (_firstNode == null)
            {
                _firstNode = node;

            }
            else
            {
                while (node.NextNode != null)
                {
                    
                }

            }

            return item;
        }
    }
}
