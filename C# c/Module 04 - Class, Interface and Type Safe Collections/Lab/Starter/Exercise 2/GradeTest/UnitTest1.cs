﻿using System;
using System.Security.Cryptography.X509Certificates;
using GradesPrototype.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GradeTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestInitialize]
        public void init()
        {
            GradesPrototype.Data.DataSource.CreateData();
        }

        string assessmentDate = "01-01-2012";

        string badAssessmentDate = "01-01-2017";


        [TestMethod]
        public void TestValidGrade()
        {

           

            Grade grade = new Grade(1, assessmentDate, "Math", "A-", "");
            Assert.AreEqual(grade.AssessmentDate, assessmentDate);
            Assert.AreEqual(grade.SubjectName, "Math");
            Assert.AreEqual(grade.Assessment, "A-");
            Assert.AreEqual(grade.StudentID, 1);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestBadDate()
        {
            Grade grade = new Grade(1, "01-01-2323", "Math", "A-", "");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestDateNotRecognized()
        {
            Grade grade = new Grade(1, "2323", "Math", "A-", "");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void TestBadAssessment()
        {
            Grade grade = new Grade(1, "01-01-2012", "Math", "56", "");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestBadSubject()
        {
            Grade grade = new Grade(1, "01-01-2012", "IT", "A+", "");
        }
    }
}
