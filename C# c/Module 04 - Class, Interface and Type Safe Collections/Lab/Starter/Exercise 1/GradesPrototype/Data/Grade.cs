﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GradesPrototype.Data
{
    // Types of user
    public enum Role { Teacher, Student };

    // WPF Databinding requires properties

    // TODO: Exercise 1: Task 1a: Convert Grade into a class and define constructors
    public class Grade
    {
        public int StudentID { get; set; }
        private string _assessmentDate;
        public string AssessmentDate
        {
            get
            {
                return _assessmentDate;
            }

            set
            {
                DateTime assessmentDate;

                // Verify that the user has provided a valid date
                if (DateTime.TryParse(value, out assessmentDate))
                {
                    // Check that the date is no later than the current date.                    
                    if (assessmentDate > DateTime.Now)
                    {
                        // Throw an ArgumentOutOfRangeException if the date is after the current date
                        throw new ArgumentOutOfRangeException("AssessmentDate", "Assessment date must be on or before the current date");
                    }

                    // If the date is valid, then save it in the appropriate format.
                    _assessmentDate = assessmentDate.ToString("d");
                }
                else
                {
                    // If the date is not in a valid format then throw an ArgumentException
                    throw new ArgumentException("AssessmentDate", "Assessment date is not recognized");
                }
            }
        }
        public string SubjectName { get; set; }
        public string Assessment { get; set; }
        public string Comments { get; set; }

        public Grade(int studentId, string assessmentDate, string subjectName, string assessment, string comments)
        {
            StudentID = studentId;
            AssessmentDate = assessmentDate;
            SubjectName = subjectName;
            Assessment = assessment;
            Comments = comments;
        }

        public Grade()
        {
            StudentID = 0;
            AssessmentDate = DateTime.Now.ToString("d");
            SubjectName = "Math";
            Assessment = "A";
            Comments = "";
        }
    }

    // TODO: Exercise 1: Task 2a: Convert Student into a class, make the password property write-only, add the VerifyPassword method, and define constructors
    public class Student
    {
        public int StudentID { get; set; }
        public string UserName { get; set; }
        private string _password = Guid.NewGuid().ToString();
        public string Password
        {
            set => _password = value;
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int TeacherID { get; set; }


        public Student(int studentId, string userName, string password, string firstName, string lastName, int teacherId)
        {
            StudentID = studentId;
            UserName = userName;
            Password = password;
            FirstName = firstName;
            LastName = lastName;
            TeacherID = teacherId;
        }

        public bool VerifyPassword(string password)
        {
            return (String.Compare(_password, password) == 0);
        }

        public Student()
        {
            var guidGenerator = Guid.NewGuid();

            StudentID = 0;
            UserName = "";
            Password = "";
            FirstName = "";
            LastName = "";
            TeacherID = 0;

        }
    }


    // TODO: Exercise 1: Task 2b: Convert Teacher into a class, make the password property write-only, add the VerifyPassword method, and define constructors
    public class Teacher
    {
        public int TeacherID { get; set; }
        public string UserName { get; set; }
        private string _password = Guid.NewGuid().ToString();
        public string Password
        {
            set => _password = value;
        }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Class { get; set; }


        public Teacher(int teacherId, string userName, string password, string firstName, string lastName, string @class)
        {
            TeacherID = teacherId;
            UserName = userName;
            Password = password;
            FirstName = firstName;
            LastName = lastName;
            Class = @class;
        }

        public bool VerifyPassword(string password)
        {
            return (String.Compare(_password, password) == 0);
        }

        public Teacher()
        {
            TeacherID = 0;
            UserName = "";
            Password = "";
            FirstName = "";
            LastName = "";
            Class = "";
        }
    }
}
