﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;
using System.Data.Services.Common;
using System.Data.Services;
using Grades.DataModel;


namespace Grades.Web.Services
{
    //TODO: Excersice 1: Task 2a: Replace the object keyword with your data source class name.
    public class GradesWebDataService : DataService<SchoolGradesDBEntities>
    {
        public static void InitializeService(DataServiceConfiguration config)
        {
            config.SetServiceOperationAccessRule("StudentInclass", ServiceOperationRights.AllRead);
            //TODO: Excersice 1: Task 2b: set rules to indicate which entity sets and service operations are visible, updatable, etc

            var roles = new string[5] { "Grade", "Teacher", "Student", "Subject", "User" };

            foreach (var role in roles)
            {
                config.SetEntitySetAccessRule(role, EntitySetRights.All);
            }
        }

        [WebGet]
        public IEnumerable<Student> studentsInClass(string className)
        {
            var students = from Student s in this.CurrentDataSource.Students
                           where String.Equals(s.Teacher.Class, className)
                           select s;
            return students;
        }
    }
}
