﻿using SticksGame.game;
using SticksGame.players;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SticksGame
{
    class Program
    {
        static void Main(string[] args)
        {
            IPlayer player1 = new SmartPlayer(1);
            IPlayer player2 = new RandomPlayer(2);
            IGame game = new Game(player1, player2);
            game.Init();
            do
            {
                game.Play();
                Console.WriteLine(game.getStats());
                Console.WriteLine("Play again? (y/n) ");
            } while (Console.ReadLine() != "n");
        }
    }
}
