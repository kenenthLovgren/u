﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SticksGame.players
{
    class HumanPlayer : RandomPlayer
    {
        public HumanPlayer(int index)
            : base(index)
        {
            Console.Write("What is your name? ");
            Name = Console.ReadLine();
        }

        public override int GetMove(int RemainingSticks)
        {
            int move = 0;
            do
            {
                Console.Write("Sticks remaining: " + RemainingSticks + ", your move: ");
                String moveStr = Console.ReadLine();
                try
                {
                    move = Convert.ToInt32(moveStr);
                }
                catch
                {
                    move = 0;
                }
            } while (move < 1 || move > RemainingSticks || move > 3);
            
            return move;
        }
    }
}
