﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SticksGame.players
{
    class SmartPlayer : RandomPlayer
    {
        public SmartPlayer(int index)
            : base(index)
        {
            Name = "Smart player (" + index + ")";
        }

        public override int GetMove(int RemainingSticks)
        {
            int move = 0;
            if (RemainingSticks > 13)
            {
                move = RemainingSticks - 13;
            }
            else if (RemainingSticks > 9)
            {
                move = RemainingSticks - 9;
            }
            else if (RemainingSticks > 5)
            {
                move = RemainingSticks - 5;
            }
            else if (RemainingSticks > 1)
            {
                move = RemainingSticks - 1;
            }

            if (move < 1 || move > 3)
            {
                move = base.GetMove(RemainingSticks);
            }
            return move;
        }
    }
}
