﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SticksGame.players
{
    class EnSten : RandomPlayer
    {
        private int[][] _learned;
        private List<int[]> movesInGame;

        public EnSten(int index)
            : base(index)
        {
            Name = "EnSten player (" + index + ")";
        }

        public override void Init()
        {
            _learned = new int[15][];
            for (int i = 0; i < 15; i++)
            {
                _learned[i] = new int[3];
                for (int j = 0; j < 3; j++)
                {
                    _learned[i][j] = 0;
                }
            }
        }

        public override void StartGame()
        {
            movesInGame.Clear();
        }

        public override int GetMove(int RemainingSticks)
        {
            bool[] moves = new bool[3];

            int bestCount = _learned[RemainingSticks - 1][0];

            return -1;
        }
    }
}
