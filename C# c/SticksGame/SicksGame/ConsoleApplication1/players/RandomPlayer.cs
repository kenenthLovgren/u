﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SticksGame.players
{
    class RandomPlayer :StupidPlayer
    {
        private static Random rnd = new Random();

        public RandomPlayer(int Index) : base(Index) {
            Name = "Random Player (" + Index + ")";
        }

        public override int GetMove(int RemainingSticks)
        {
            return rnd.Next(RemainingSticks > 3 ? 3 : RemainingSticks) + 1;
        }
    }
}
