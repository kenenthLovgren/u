﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SticksGame.players
{
    class StupidPlayer : IPlayer
    {
        private string _name;
        public string Name
        {
            get { return _name; }
            protected set { _name = value; }
        }

        private int _wins;
        public int Wins
        {
            get { return _wins; }
            private set { _wins = value; }
        }

        public StupidPlayer(int index)
        {
            Name = "Stupid Player (" + index + ")";
            Wins = 0;
        }

        public virtual void Init()
        {
            // Nothing to do
        }

        public virtual void StartGame()
        {
            // Nothing to do
        }

        public virtual int GetMove(int RemainingSticks)
        {
            return 1;
        }

        public virtual void EndGame(bool YouWin)
        {
            if (YouWin)
                Wins++;
        }
    }
}
