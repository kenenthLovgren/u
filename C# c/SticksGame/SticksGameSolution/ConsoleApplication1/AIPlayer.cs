﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SticksGame
{
    class AIPlayer : RandomPlayer
    {
        Dictionary<Int32,Int32> moveHistory = null;
        Int32[,] knowledge = new Int32[15,3];

        public AIPlayer() : base()
        {
            Name += "SpookyAI";
            for(int sticks=0; sticks<15; sticks++) {
                for(int moves = 0; moves<3; moves++)
                    knowledge[sticks,moves] = 0;
            }
        }

        public override void EndGame(bool isWinner) {
            base.EndGame(isWinner);
            int sticks = 15;
            foreach(var entry in moveHistory) {
                if (sticks > entry.Key)
                {   // his move!
                    if (!isWinner)
                        knowledge[sticks - 1, sticks - entry.Key - 1]++;
                    else
                        knowledge[sticks - 1, sticks - entry.Key - 1]--;
                };
                // my move
                if (isWinner)
                    knowledge[entry.Key - 1, entry.Value - 1]++;
                else
                    knowledge[entry.Key - 1, entry.Value - 1]--;
                sticks = entry.Key-entry.Value;
            }
            if(sticks > 0) {    // looser took remaining sticks!
                knowledge[sticks - 1, sticks - 1]--;
            }
            Console.WriteLine(knowledgeToString());
            moveHistory = null;
        }

        private string knowledgeToString()
        {
            StringBuilder sb = new StringBuilder();
            for (int move = 1; move < 4; move++)
            {
                sb.Append(move + ":[");
                for (int sticks = 1; sticks < 16; sticks++)
                    sb.Append(knowledge[sticks - 1, move - 1] + ", ");
                sb.Append("]\n");
            }
            return sb.ToString();
        }

        public override void StartGame()
        {
            base.StartGame();
            if (moveHistory == null)
                moveHistory = new Dictionary<int, int>();
        }
        public override int GetMove(int sticks)
        {
 	        int move = 1;

            if(1 < sticks && knowledge[sticks-1,move-1] < knowledge[sticks-1,2-1]) move = 2;
            if(2 < sticks && knowledge[sticks-1,move-1] < knowledge[sticks-1,3-1]) move = 3;

            moveHistory.Add(sticks, move);

            return move;
        }
    }
}
