﻿using System;

namespace SticksGame
{
    public class Player
    {
        private int _wins;

        public Player() {
            Name = "Simple";
            _wins = 0;
        }

        public string Name { get; set; }

        public int Wins
        {
            get { return _wins; }
            private set { _wins = value; }
        }

        public virtual int GetMove(int sticks) {
            return 1;
        }

        public virtual void StartGame() {
        }
        public virtual void EndGame(bool isWinner)
        {
            if (isWinner) Wins++;
        }
    }
}
