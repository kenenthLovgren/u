﻿using System;

namespace SticksGame
{
    class HumanPlayer : Player
    {
        public HumanPlayer() : base()
        {
            Console.Write("\nCreating human player\n\nEnter player name: ");
            Name = Console.ReadLine();
        }

        public override int GetMove(int sticks)
        {
            int move = 0;
            int maxMove = (sticks < 3 ? sticks : 3);
            do
            {
                Console.Write(sticks + " remaining, move (1-" + (sticks < 3 ? sticks : 3) + "): ");
                string movetxt = Console.ReadLine();
                try
                {
                    move = Int32.Parse(movetxt);
                }
                catch (Exception)
                {
                };
            } while (move < 1 || move > maxMove);
            return move;
        }

        public override void EndGame(bool isWinner)
        {
            base.EndGame(isWinner);
            Console.WriteLine("{0} says {1}!!", Name, isWinner ? "YEEES!" : "Sigh!");
        }
    }
}
