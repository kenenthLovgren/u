﻿using System;

namespace SticksGame
{
    class RandomPlayer : Player
    {
        private Random myRand;

        public RandomPlayer() : base()
        {
            Name = Name + "Random";
            myRand = new Random();
        }

        public override int GetMove(int sticks)
        {
            if (sticks < 1) throw new Exception("Less than 1 sticks");
            return myRand.Next(1, sticks > 3 ? 3 : sticks);
        }
    }
}
