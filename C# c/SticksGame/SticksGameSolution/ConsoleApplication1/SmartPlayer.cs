﻿
namespace SticksGame
{
    class SmartPlayer : RandomPlayer
    {
        public SmartPlayer() : base()
        {
            Name += "Smart";
        }

        public override int GetMove(int sticks)
        {
            int move = (sticks - 1) % 4;
            if (move < 1)
                move = base.GetMove(sticks);
            return move;
        }
    }
}
