﻿using System;

namespace SticksGame
{
    
    class Game
    {
        private Player[] players = new Player[2];

        public Game() { }

        public void Init()              // initialisering af spillere og spil
        {
            players[0] = Program.CreatePlayer(0);
            players[1] = Program.CreatePlayer(1);
        }

        public void Play()              // afvikling af eet spil
        {
            int sticks;
            int current;

            players[0].StartGame();
            players[1].StartGame();

            for (sticks = 15, current = 0; sticks > 0; current = 1 - current)
            {
                Console.WriteLine("{0} sticks remaining, {1}'s turn to move", sticks, players[current].Name);
                sticks -= players[current].GetMove(sticks);
            }

            Console.WriteLine("{0} sticks remaining, {1} have won!", sticks, players[current].Name);
            players[0].EndGame(0 == current);
            players[1].EndGame(1 == current);

        }
        
        public void Play(int gameCount) // afvikling af gameCount spil
        {
            if(gameCount < 0)
                throw new ArgumentException("gameCount negative");
            for (int i = 0; i < gameCount; ++i)
                Play();
        }
        
        public string GetStat()         // returnerer spilstatistik
        {
            return String.Format("{0} games played, score {1}-{2}",
                (players[0].Wins + players[1].Wins), players[0].Wins, players[1].Wins);
        }
    }
}
