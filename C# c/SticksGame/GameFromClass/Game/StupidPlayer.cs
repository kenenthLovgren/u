﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    public class StupidPlayer : IPlayer
    {
        string _name;
        public string Name {
            get { return _name; }
            protected set { _name = value; }
        }
        public StupidPlayer(int id)
        {
            Name = "Stupid("+id+")";
        }

        public virtual int GetMove(int sticks) {
            return 1;
        }
    }
}
