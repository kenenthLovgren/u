﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVCPractice.Models
{
    public class Opera
    {
        public int OperaId { get; set; }

        [Required]
        [StringLength(200)]
        public string Title { get; set; }
        [CheckValidYear]
        public int Year { get; set; }
        [Required]
        public string Composer { get; set; }


    }

    public class CheckValidYear : ValidationAttribute
    {
        public CheckValidYear()
        {
            ErrorMessage = "The year is earlier than 1598, which is the year the first opera was written: Daphne, 1598. " + "by corsi, peri and Rinuccini";
        }
        public override bool IsValid(object value)
        {
            
            int year = (int)value;
            if (year < 1598)
            {
                return false;
            }

            return true;
        }
    }
}