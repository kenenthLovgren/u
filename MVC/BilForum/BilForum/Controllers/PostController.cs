﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BilForum.Models;

namespace BilForum.Controllers
{
    public class PostController : Controller
    {
        BilForumDB db = new BilForumDB();
     
        // GET: Post
        public ActionResult Index()
        {
            var posts = db.Posts.ToList<PostModel>();
            return View(posts);
        }

        public ActionResult Showpost(int id)
        {
            var selectedPost = (from post in db.Posts
                                where post.ID == id
                                select post).FirstOrDefault();

            if (selectedPost != null)
                return View(selectedPost);
            return RedirectToAction("Index");
        }
        // GET //
        public ActionResult Create()
        {
            PostModel newPost = new PostModel();

            return View(newPost);
        }
        [HttpPost]
        public ActionResult Create(PostModel post)
        {
            if (!ModelState.IsValid)
            {
                return View("Create", post);
            }
            else
            {
                if (post != null)
                {
                    //TODO: Some stuff here
                    PostModel CreatePost = new PostModel();

                    CreatePost.Topic = post.Topic;
                    CreatePost.Author = post.Author;
                    CreatePost.Subject = post.Subject;
                    CreatePost.Content = post.Content;


                }

                db.Posts.Add(post);
                db.SaveChanges();
                return RedirectToAction("/Post");
            }


        }

        public ActionResult DetailsByTitle(string title)
        {
            PostModel forum = (PostModel)(from o in db.Posts
                                          where o.Subject == title
                                          select o).FirstOrDefault();
            if (forum == null)
            {
                return HttpNotFound();
            }

            return View("ShowPost", title);
        }
    }
}