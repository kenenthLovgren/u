﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace BilForum.Models
{
    public class BilForumDB : DbContext
    {
        public DbSet<PostModel> Posts { get; set; }
        public DbSet<TopicModel> Topics { get; set; }
    }
}