﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace BilForum.Models
{
    public class BilForumDBInitializer : DropCreateDatabaseAlways<BilForumDB>
    {

        protected override void Seed(BilForumDB context)
        {
            context.Posts.Add(new PostModel
            {
                Subject = "Modeller",
                Content = "Her er nogle fede biler",
                Author = "Sidney Lee",
                CreatedDate = DateTime.Today,
                Topic = new TopicModel { TopicName = "Fede Biler" }
            });

            context.Posts.Add(new PostModel
            {
                Subject = "Hår på F*****",
                Content = "Vi har samlet alle brians citater igennem årnene",
                Author = "Brian Nielsen",
                CreatedDate = DateTime.Today,
                Topic = new TopicModel { TopicName = "Random" }
            });
            context.SaveChanges();
            base.Seed(context);

        }
    }
}