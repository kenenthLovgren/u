﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BilForum.Models
{
    public class PostModel
    {
        public int ID { get; set; }
        [DisplayName("User")]
        public string Author { get; set; }
        public string Subject { get; set; }
        public DateTime CreatedDate { get; set; }
        [DataType(DataType.MultilineText)]
        public string Content { get; set; }

        public int TopicId { get; set; }
        public TopicModel Topic { get; set; }
    }
}