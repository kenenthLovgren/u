﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BilForum.Models
{
    public class TopicModel
    {
        public int ID { get; set; }
        public string TopicName { get; set; }
    }
}