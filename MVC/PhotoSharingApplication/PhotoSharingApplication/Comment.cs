﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PhotoSharingApplication
{
    public class Comment
    {
        public int CommentID { get; set; }
        public int PhotoID { get; set; }
        public string Username { get; set; }
        [MaxLength(250)]
        public string Subject { get; set; }
        [DataType(DataType.MultilineText)]
        [DisplayName("Comment")]
        public string Body { get; set; }
        public virtual Photo Photo { get; set; }
    }
}