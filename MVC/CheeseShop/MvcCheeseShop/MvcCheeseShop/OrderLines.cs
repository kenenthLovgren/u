namespace MvcCheeseShop.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class OrderLines
    {
        [Key]
        public int orderLineId { get; set; }

        public int orderId { get; set; }

        public int productId { get; set; }

        public double weight { get; set; }

        public virtual Order Order { get; set; }

        public virtual Product Product { get; set; }
    }
}
