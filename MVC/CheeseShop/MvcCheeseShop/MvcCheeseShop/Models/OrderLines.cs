﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcCheeseShop.Models
{
    public partial class OrderLines
    {
        public int orderLineId { get; set; }
        public int orderId { get; set; }
        public int productId { get; set; }
        public double weight { get; set; }
    
        public virtual Order Order { get; set; }
        public virtual Product Product { get; set; }
    }
}