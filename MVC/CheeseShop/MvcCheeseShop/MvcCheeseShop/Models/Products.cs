﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MvcCheeseShop.Models
{
    public partial class Products
    {

        public int ProductId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Pictures { get; set; }
        public string Variant { get; set; }
        public string Type { get; set; }
        public string Supplier { get; set; }
        public string Brand { get; set; }
        public string Country { get; set; }
        public double? FatProcentage { get; set; }
        public int? Aging { get; set; }
        public bool? Moldy { get; set; }
        public string Color { get; set; }
        public string Firmness { get; set; }
        public double? KgPrice { get; set; }
        public virtual ICollection<OrderLine> OrderLines { get; set; }

    }

    public class CheeseDBContext : DbContext
    {

        public DbSet<Products> Products { get; set; }

    }
}