﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcCheeseShop.Models
{
    public partial class Order
    {
        public Order()
        {
            this.OrderLines = new HashSet<OrderLine>();
        }

        public int orderId { get; set; }
        public int customerId { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual ICollection<OrderLine> OrderLines { get; set; }
    }
}