﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MvcCheeseShop;

namespace MvcCheeseShop.Controllers
{
    public class OrderViewsController : Controller
    {
        private CheeseEntities db = new CheeseEntities();

        // GET: OrderViews
        public ActionResult Index()
        {
            return View(db.OrderViews.ToList());
        }

        // GET: OrderViews/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrderView orderView = db.OrderViews.Find(id);
            if (orderView == null)
            {
                return HttpNotFound();
            }
            return View(orderView);
        }

        // GET: OrderViews/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: OrderViews/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "customerId,orderId,eMail,Samlet_pris")] OrderView orderView)
        {
            if (ModelState.IsValid)
            {
                db.OrderViews.Add(orderView);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(orderView);
        }

        // GET: OrderViews/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrderView orderView = db.OrderViews.Find(id);
            if (orderView == null)
            {
                return HttpNotFound();
            }
            return View(orderView);
        }

        // POST: OrderViews/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "customerId,orderId,eMail,Samlet_pris")] OrderView orderView)
        {
            if (ModelState.IsValid)
            {
                db.Entry(orderView).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(orderView);
        }

        // GET: OrderViews/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrderView orderView = db.OrderViews.Find(id);
            if (orderView == null)
            {
                return HttpNotFound();
            }
            return View(orderView);
        }

        // POST: OrderViews/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            OrderView orderView = db.OrderViews.Find(id);
            db.OrderViews.Remove(orderView);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
