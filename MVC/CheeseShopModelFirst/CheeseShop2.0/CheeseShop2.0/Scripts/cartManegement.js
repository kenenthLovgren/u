﻿function addToCart(id) {
    var cart = JSON.parse(sessionStorage.getItem("cart"));
    var quantityTextBox = document.getElementById("quantityTextBox"+id);
    var quantity = Number(quantityTextBox.value);
    if ((!quantity) || (quantity < 0)) {
        alert("You must input a positive value!");
        return;
    }
    if (!cart) {
        cart = [];
    }

    var curPriceTag = document.getElementById("currentPriceTag"+id);
    var price = Number(curPriceTag.innerText);

    cart[cart.length] = [id,quantity,price];

    console.log("Product " +id +" in cart. Quantity: "+quantity+", at a price of "+price+" Bitcoins!");
    sessionStorage.setItem("cart",JSON.stringify(cart));
}

function cartNotEmpty() {
    var cart = JSON.parse(sessionStorage.getItem("cart"));
    return (cart) && (cart.length>0);
}

function updateCurrentPrice(id) {
    var inputBox = document.getElementById("quantityTextBox"+id);
    if(!inputBox)
        return;
    var value = Number(inputBox.value);
    if (!value) {
        value = Number(inputBox.tag);
        if(!value)
            return;
        inputBox.value = value;
    }
    var curPriceTag = document.getElementById("currentPriceTag"+id);
    if(!curPriceTag)
        return;
    var kgPriceElement = document.getElementById("priceTag"+id);
    if(!kgPriceElement)
        return;
    var kgPriceString = kgPriceElement.innerText;
    kgPriceString = kgPriceString.replace(",",".");
    var kgPrice = Math.round(Number(kgPriceString)*100)/100;
    if(!kgPrice)
        return;
    curPriceTag.innerHTML = Math.round(value*kgPrice*100)/100;
}

function cartMouseOver() {
    $("#cartIcon").attr("src","~/Resources/ShoppingCartHover.svg");
}

function cartMouseOut() {
    $("#cartIcon").attr("src","~/Resources/ShoppingCart.svg");
}

function gotoCart() {
    var oldUrl = "/checkout/"+(sessionStorage.getItem("cart"));
    var newUrl = oldUrl.replace(","," ");
    while (oldUrl !== newUrl) {
        oldUrl = newUrl;
        newUrl = oldUrl.replace(","," ");
    }
    oldUrl = newUrl;
    newUrl = oldUrl.replace(".",",");
    while (oldUrl !== newUrl) {
        oldUrl = newUrl;
        newUrl = oldUrl.replace(".",",");
    }
    console.log(newUrl);
    location.href = newUrl;
    //var dummy = document.getElementById("cartDummyDiv");
    //dummy.tag = sessionStorage.getItem("cart");
    //ViewBag.cartInfo = sessionStorage.getItem("cart");

}
