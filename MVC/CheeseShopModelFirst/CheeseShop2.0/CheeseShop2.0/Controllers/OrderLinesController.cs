﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CheeseShop.Models;

namespace CheeseShop.Controllers
{
    public class OrderLinesController : Controller
    {
        private CheeseDB db = new CheeseDB();

        // GET: OrderLines
        public ActionResult Index()
        {
            var orderLines = db.OrderLines.Include(o => o.Order).Include(o => o.Product);
            return View(orderLines.ToList());
        }

        // GET: OrderLines/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrderLines orderLines = db.OrderLines.Find(id);
            if (orderLines == null)
            {
                return HttpNotFound();
            }
            return View(orderLines);
        }

        // GET: OrderLines/Create
        public ActionResult Create()
        {
            ViewBag.OrdersId = new SelectList(db.Orders, "Id", "Id");
            ViewBag.ProductsId = new SelectList(db.Products, "Id", "Title");
            return View();
        }

        // POST: OrderLines/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,OrdersId,ProductsId,Weight")] OrderLines orderLines)
        {
            if (ModelState.IsValid)
            {
                db.OrderLines.Add(orderLines);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.OrdersId = new SelectList(db.Orders, "Id", "Id", orderLines.OrdersId);
            ViewBag.ProductsId = new SelectList(db.Products, "Id", "Title", orderLines.ProductsId);
            return View(orderLines);
        }

        // GET: OrderLines/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrderLines orderLines = db.OrderLines.Find(id);
            if (orderLines == null)
            {
                return HttpNotFound();
            }
            ViewBag.OrdersId = new SelectList(db.Orders, "Id", "Id", orderLines.OrdersId);
            ViewBag.ProductsId = new SelectList(db.Products, "Id", "Title", orderLines.ProductsId);
            return View(orderLines);
        }

        // POST: OrderLines/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,OrdersId,ProductsId,Weight")] OrderLines orderLines)
        {
            if (ModelState.IsValid)
            {
                db.Entry(orderLines).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.OrdersId = new SelectList(db.Orders, "Id", "Id", orderLines.OrdersId);
            ViewBag.ProductsId = new SelectList(db.Products, "Id", "Title", orderLines.ProductsId);
            return View(orderLines);
        }

        // GET: OrderLines/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrderLines orderLines = db.OrderLines.Find(id);
            if (orderLines == null)
            {
                return HttpNotFound();
            }
            return View(orderLines);
        }

        // POST: OrderLines/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            OrderLines orderLines = db.OrderLines.Find(id);
            db.OrderLines.Remove(orderLines);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
