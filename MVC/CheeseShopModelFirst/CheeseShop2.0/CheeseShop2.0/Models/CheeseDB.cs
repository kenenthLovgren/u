﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace CheeseShop.Models
{
    public class CheeseDB : DbContext
    {

        public DbSet<Customers> Customers {get; set;}
        public DbSet<Orders> Orders {get; set;}
        public DbSet<Products> Products {get;set;}
        public DbSet<OrderLines> OrderLines {get; set;}
        
        
    }
}