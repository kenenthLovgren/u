﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CheeseShop.Models
{
    public class Orders
    {
        public int Id { get; set; }
        [Required]
        public int CustomersId { get; set; }

        public virtual Customers Customer{get; set;}
        public virtual ICollection<OrderLines> OrderLines {get; set;}




    }
}