﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.IO;

namespace CheeseShop.Models.InitializeData
{
    public class InitializeCheeseDB : DropCreateDatabaseIfModelChanges<CheeseDB>
    {
        protected override void Seed(CheeseDB context)
        {
            int nCustomers = 500;
			int nOrders = 100;
			int nOrderLines = 150;
			int nProducts = 10;
			
			List<Customers> customers = generateCustomers(nCustomers);
			
            customers.ForEach(s => context.Customers.Add(s));
			Random rng = new Random();
			
            context.Products.Add(new Products()
            {
                Title = "Young Gouda Cheese | Premium Quality",
                Description = "Young cheese is a sort of cheese that has not had a lot of time to mature: it is very ‘young’. The maturing period is part of the process creating cheese, and new cheese only lays in a warehouse for a few weeks, no more than four. Young Gouda cheese nice and soft with a smooth taste. The Young Gouda cheese is an extra quality cheese with less salt.",
                Pictures = "1.jpg",
                Type = "Gouda",
                Supplier = "GaudaCheeseShop.com",
                Brand = "GaudaCheeseShop",
                FatPrecentage = 32.5,
                Country = "Netherlands",
                Aging = 3,
                Moldy = false,
                Color = "Yellow",
                Firmness ="Firm",
                KgPrice = 90
            });

            context.Products.Add(new Products()
            {
                Title = "Matured Gouda Cheese | Premium Quality",
                Description = "Matured Gouda cheese has a maturation period of 16 to 18 weeks. Because of this its taste is somewhat stronger than that of young Gouda cheese and semi-matured Gouda cheese, but it is still milder in flavour than aged cheese. Rather, the flavour of our matured cheese can be described as full and creamy.",
                Pictures = "2.jpg",
                Type = "Gouda",
                Supplier = "GaudaCheeseShop.com",
                Brand = "GaudaCheeseShop",
                FatPrecentage = 32.5,
                Country = "Netherlands",
                Aging = 4,
                Moldy = false,
                Color = "Yellow",
                Firmness ="Firm",
                KgPrice = 90
            });

            context.Products.Add(new Products()
            {
                Title = "Old Amsterdam Cheese | Premium Quality",
                Description = "Enjoying full flavor, Old Amsterdam will give you just that. The special flavor of Old Amsterdam is the result of a combined passion for making cheese and special flavors. This Old Amsterdam cheese has been awarded in 2014 with the Superior Taste Award.",
                Pictures = "3.jpg",
                Type = "Gouda",
                Supplier = "GaudaCheeseShop.com",
                Brand = "GaudaCheeseShop",
                FatPrecentage = 32.5,
                Country = "Netherlands",
                Aging = 8,
                Moldy = false,
                Color = "Nut-orange",
                Firmness ="Hard",
                KgPrice = 140
            });

            context.Products.Add(new Products()
            {
                Title = "Brie de Meaux AOC",
                Description = "Brie is certainly the most famous French cheese but with being nine time winner of Medaille DOr, Brie de Meaux can be classed as the “King of Brie”.  Brie de Meaux stands out with its smooth, voluptuous, but not quite runny texture and its creamy taste. For our customers, we chose to bring in this exquisite classic cheese made by the Dongé  family in Ile de France outside Paris. Etienne Donge set up the dairy in 1930 and his great grandchildren Luc and Jean-Michel run it today. They follow very traditional methods hand-ladle the curd and make a classic full flavoured vegetabley Brie The aromas of Brie de Meaux are milky and rich - reflecting the high quality milk used during production. Flavors are sweet and buttery with notes of mushrooms or truffles and almonds. This cheese pairs perfectly with Champagne, a red Bordeaux or Bourgogne (Burgundy).",
                Pictures = "4.jpg",
                Type = "Brie",
                Supplier = "http://www.cheeseshop.sg",
                Brand = "Dongé",
                FatPrecentage = 30,
                Country = "France",
                Aging = 1,
                Moldy = true,
                Color = "White",
                Firmness ="Soft",
                KgPrice = 430
            });

            context.Products.Add(new Products()
            {
                Title = "Cheddar Cheese 5 Year Old Extra Sharp",
                Description = "Wisconsin is leading in Cheddar production, which makes it no surprise that this Cheddar is one of our most popular cheeses. This Wisconsin Cheddar has been aged for over 5 years, giving it a rich flavor and slightly tangy bite. As cheddar ages it loses its smooth, firm texture and becomes more granular and crumbly. This is a traditional Wisconsin golden colored cheddar. The sharp flavor makes this cheese perfect for any beer or wine tasting, gives sandwiches a delicious zing, and can even be paired perfectly with gourmet sausage.",
                Pictures = "5.jpg",
                Type = "Chedder",
                Supplier = "www.wisconsincheesemart.com",
                Brand = "Wisconsincheesemart",
                FatPrecentage = 34,
                Country = "United States of America",
                Aging = 60,
                Moldy = false,
                Color = "Orange",
                Firmness ="Medium",
                KgPrice = 170
            });

            context.Products.Add(new Products()
            {
                Title = "Dunbarton Blue Cheese",
                Description = "Made by Roelli Cheese in Shullsburg, Wisconsin. This cutting edge Wisconsin Cheese combines all of the greatness of a fine English Cheddar with the bite and creaminess of a French Blue. This Wisconsin Cheese is open air aged in a cellar to enhance its rich flavor. This Wisconsin cheese strikes the perfect balance between sharpness and the creamy texture.",
                Pictures = "6.jpg",
                Type = "Blue cheese",
                Supplier = "www.wisconsincheesemart.com",
                Brand = "Wisconsincheesemart",
                FatPrecentage = 29,
                Country = "United States of America",
                Aging = 4,
                Moldy = true,
                Color = "Yellow blue",
                Firmness ="Hard",
                KgPrice = 200
            });

            context.Products.Add(new Products()
            {
                Title = "Parmesan Cheese",
                Description = "Sartori Parmesan is an Italian style hard cheese made from cows milk. Full flavored, buttery, nutty, and slightly sweet, this Wisconsin Cheese has full bodied flavor in addition to a pleasing grainy texture. Highly recommended grated for pasta dishes, spaghetti sauces, pasta fillings, pasta salads, pizza and last, but not least, salad.",
                Pictures = "7.jpg",
                Type = "Permasan",
                Supplier = "www.wisconsincheesemart.com",
                Brand = "Wisconsincheesemart",
                FatPrecentage = 29,
                Country = "Italy",
                Aging = 12,
                Moldy = false,
                Color = "Yellow",
                Firmness ="Brittle",
                KgPrice = 90
            });

            context.Products.Add(new Products()
            {
                Title = "Hi Temp Jalapeno Cheddar",
                Description = "Add jalapeno cheese to sausage and more without melting. Hi Temp does not melt like normal cheese during cooking and smoking and adds a cheesy and creamy taste to any sausage. Use 1 lb of cheese for every 10 lb of meat.",
                Pictures = "8.jpg",
                Type = "Chedder",
                Supplier = "www.wisconsincheesemart.com",
                Brand = "Wisconsincheesemart",
                FatPrecentage = 17,
                Country = "United States of America",
                Aging = 12,
                Moldy = false,
                Color = "Yellow",
                Firmness ="Firm",
                KgPrice = 97
            });

            context.Products.Add(new Products()
            {
                Title = "Drunken Goat",
                Description = "The Drunken Goat is a pasteurized goats milk cheese from the Murcia region of Spain. This smooth, creamy goats milk cheese is aged for a short period of time, then immersed in wine for at least 72 hours. This results in added flavor to the cheese and a vivid purple color to the rind. After the bath, The Drunken Goat cheese is aged for an additional 75 days to allow full maturation and intermingling of the flavors. The cheese has a pleasant semi-soft texture and a lingering creaminess, with a tangy, fruity finish. Not only is this cheese soaked in wine, it pairs beautifully with your favorite wine or cocktail! A favorite on cheese boards, The Drunken Goat cheese makes a bold statement with its visually striking appearance and dreamy, creamy texture.",
                Pictures = "9.jpg",
                Type = "Goat cheese",
                Supplier = "www.wisconsincheesemart.com",
                Brand = "Jumilia",
                FatPrecentage = 30,
                Country = "Spain",
                Aging = 1,
                Moldy = false,
                Color = "Yellow red",
                Firmness ="Medium",
                KgPrice = 146
            });

            context.Products.Add(new Products()
            {
                Title = "Gouda Cheese Marieke Bacon",
                Description = "Hollands Family Cheeses award winning Marieke Gouda is now available with bacon. Hollands Family Cheese follows an authentic old world gouda recipe, importing its equipment and cultures from cheesemaker Marieke Pentermans native Holland to the farmstead operation in Thorp, Wisconsin. This new variety incorporates a local Wisconsin made product, Nolecheks bacon, also produced in Thorp. The result is a creamy, slightly sweet raw milk gouda cheese studded with real smoky bacon.",
                Pictures = "10.jpg",
                Type = "Gouda",
                Supplier = "www.wisconsincheesemart.com",
                Brand = "Marieke",
                FatPrecentage = 30,
                Country = "Netherlands",
                Aging = 15,
                Moldy = false,
                Color = "Yellow orange",
                Firmness ="Soft",
                KgPrice = 179
            });
            context.SaveChanges();

            for(int i = 0; i < nOrders;i++){
				context.Orders.Add(new Orders(){
					CustomersId = rng.Next(nCustomers)+1
				});
			}
            context.SaveChanges();


			for(int i = 0; i < nOrderLines; i++){
				context.OrderLines.Add(new OrderLines(){
					OrdersId = rng.Next(nOrders)+1,
					ProductsId = rng.Next(nProducts)+1,
					Weight = ((double) rng.Next(100000)+1)/1000.0
				});
			}
			
            context.SaveChanges();

        }
		
		public List<Customers> generateCustomers(int nCustomers){
			List<string> firstNames = loadNamesFromFile("C:/cSharp/MVC/CheeseShopModelFirst/CheeseShop2.0/CheeseShop2.0/Resources/firstNameList.txt");
			List<string> lastNames = loadNamesFromFile("C:/cSharp/MVC/CheeseShopModelFirst/CheeseShop2.0/CheeseShop2.0/Resources/lastNameList.txt");
			List<Customers> svar = new List<Customers>();
			Random rng = new Random();
			for(int i = 0; i< nCustomers; i++){
				string fnam = firstNames[rng.Next(firstNames.Count)];
				string lnam = lastNames[rng.Next(lastNames.Count)];
				string emailPart1 = fnam;
				switch(rng.Next(3)){
					case 0:
						emailPart1 = fnam.ToLower();
						break;
					case 1:
						emailPart1 = (""+fnam[0]).ToLower();
						break;
					case 2:
						emailPart1 = fnam.Substring(0,2).ToLower();
						break;
				}
				string emailPart2 = "";
				switch(rng.Next(3)){
					case 0:
						emailPart2 = "";
						break;
					case 1:
						emailPart2 = "_";
						break;
					case 2:
						emailPart2 = ".";
						break;
				
				
				}
				string emailPart3 = lnam;
				switch(rng.Next(3)){
					case 0:
						emailPart3 = lnam.ToLower();
						break;
					case 1:
						emailPart3 = (""+lnam[0]).ToLower();
						break;
					case 2:
						emailPart3 = (""+lnam.Substring(0,2)).ToLower();
						break;	
				}
				String emailPart4 = "@home.com";
				switch(rng.Next(10)){
					case 0:
						emailPart4 = "@home.com";
						break;
					case 1:
						emailPart4 = "@gmail.com";
						break;
					case 2:
						emailPart4 = "@hotmail.com";
						break;
					case 4:
						emailPart4 = "@cheesefan.com";
						break;
					case 5:
						emailPart4 = "@cheezewiz.com";
						break;
					case 6:
						emailPart4 = "@cheese.org";
						break;
					case 7:
						emailPart4 = "@cia.gov";
						break;
					case 8:
						emailPart4 = "@mit.edu";
						break;
					case 9:
						emailPart4 = "@france.gov";
						break;
				}
				svar.Add(new Customers(){
					FirstName = fnam,
					LastName=lnam,
					Email=emailPart1+emailPart2+emailPart3+emailPart4,
				});
				
			}
			return svar;
			
		}
		
		
		public List<string> loadNamesFromFile(string fileName){
			List<string> names = File.ReadAllLines(fileName).ToList();
			for(int i = 0; i < names.Count; i++){
				names[i] = (names[i][0])+(names[i].Substring(1).ToLower());
			}
			return names;
		}
    }
}