﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CheeseShop.Models.NonDBModels
{
    public class Cart
    {
        public List<CartEntry> Entries {get;set;}

        public Cart(string json)
        {
            json = json.Substring(1,json.Length-2);
            string[] toks = json.Split('[');
            Entries = new List<CartEntry>();
            for(int i = 1; i < toks.Length; i++)
            {
                string[] toks2 = toks[i].Split(' ');
                int pos = toks2[2].IndexOf(']');
                Entries.Add(new CartEntry(){Id= int.Parse(toks2[0]), Quantity= double.Parse(toks2[1],System.Globalization.CultureInfo.GetCultureInfo(1030)), Price = double.Parse(toks2[2].Substring(0,pos),System.Globalization.CultureInfo.GetCultureInfo(1030))});
            }
        }

    }


    public class CartEntry
    {
        public int Id {get;set;}
        public double Quantity {get;set;}
        public double Price {get;set;}

    }


}