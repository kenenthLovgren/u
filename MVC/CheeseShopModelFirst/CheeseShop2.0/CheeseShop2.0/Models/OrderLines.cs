﻿using System.ComponentModel.DataAnnotations;

namespace CheeseShop.Models
{
    public class OrderLines
    {
        public int Id { get; set; }
        [Required]
        public int OrdersId { get; set; }
        [Required]
        public int ProductsId {get; set;}
        [Required]
        public double Weight { get; set; }

        public virtual Orders Order {get; set;}
        public virtual Products Product {get; set;}

    }
}