﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CheeseShop.Models
{
    public class Products
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Pictures { get; set; }
        public string Variant { get; set; }
        public string Type { get; set; }
        public string Supplier { get; set; }
        public string Brand { get; set; }
        public string Country { get; set; }
        public double? FatPrecentage { get; set; }
        public int Aging { get; set; }
        public bool? Moldy { get; set; }
        public string Color { get; set; }
        public string Firmness { get; set; }
        public decimal KgPrice { get; set; }

        public virtual ICollection<OrderLines> OrderLines {get; set;}
    }


}

   